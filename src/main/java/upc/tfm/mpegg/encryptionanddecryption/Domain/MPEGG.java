package upc.tfm.mpegg.encryptionanddecryption.Domain;

import lombok.Getter;
import lombok.Setter;
import org.xml.sax.SAXException;
import upc.tfm.mpegg.files.accessunit.AccessUnit;
import upc.tfm.mpegg.files.dataset.Dataset;
import upc.tfm.mpegg.files.datasetgroup.DatasetGroup;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.UUID;

@Getter
@Setter
public class MPEGG {
    private int id;
    private AccessUnit accessUnit;
    private Dataset dataset;
    private DatasetGroup datasetGroup;
    private byte[] key;

    public MPEGG () throws ParserConfigurationException {
    }

    public void generateKey(){
        if(accessUnit != null)  key = accessUnit.generateKey();
        if(dataset != null)  key = dataset.generateKey();
        if(datasetGroup != null)  key = datasetGroup.generateKey();
    }

    public String encryptAccessUnit(String data) throws InvalidAlgorithmParameterException,  NoSuchAlgorithmException,  NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return accessUnit.encrypt(data, key);
    }

    public String signAccessUnit() throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, ParserConfigurationException, XMLSignatureException, TransformerException, SAXException {
        return accessUnit.encryptAndSign();
    }

    public String encryptDataset(String data) throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        return dataset.encrypt(data,key);
    }
    public String signDataset() throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException {
        return dataset.encryptAndSign();
    }

    public String signDatasetGroup() throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException {
        return datasetGroup.encryptAndSign();
    }

    public String encryptDatasetGroup(String data) throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        return datasetGroup.encrypt(data, key);
    }

    public String decryptAccessUnit(String encryptedData, byte[] key) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return accessUnit.decrypt(encryptedData, key);
    }

    public String decryptDataset(String encryptedData, byte[] key) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return dataset.decrypt(encryptedData, key);
    }

    public String decryptDatasetGroup(String encryptedData, byte[] key) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return datasetGroup.decrypt(encryptedData, key);
    }
}
