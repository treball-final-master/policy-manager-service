package upc.tfm.mpegg.encryptionanddecryption.Domain;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import upc.tfm.mpegg.files.EncryptionParameter;
import upc.tfm.mpegg.files.accessunit.AccessUnitEncryptionParameters;
import upc.tfm.mpegg.files.utils.Signature;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.XMLConstants;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

public abstract class MPEGGProtectionBox {

    protected Document document;
    protected Element documentRoot;
    protected EncryptionParameter encryptionParameter;


    public byte[] generateKey() {
        return generateRandomByteArray(encryptionParameter.getCipher().getKeySize() / 8);
    }

    protected AlgorithmParameterSpec getParametersSpec() throws InvalidAlgorithmParameterException {
        String operationMode = encryptionParameter.getOperationMode();
        if( operationMode.compareTo("GCM") == 0){
            return new GCMParameterSpec(AccessUnitEncryptionParameters.GCM_TAG_LENGTH * 8, encryptionParameter.getIV());
        } else if( operationMode.compareTo("CTR") == 0 ) {
            return new IvParameterSpec(encryptionParameter.getIV());
        }
        throw new InvalidAlgorithmParameterException("Wrong operation mode");
    }

    private byte[] generateRandomByteArray(int size) {
        SecureRandom random = new SecureRandom();
        byte[] randomBytes = new byte[size];
        random.nextBytes(randomBytes);
        return randomBytes.clone();
    }

    protected Document getXmlFromString (String xmlString) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // optional, but recommended
        // process XML securely, avoid attacks like XML External Entities (XXE)
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

        // parse XML file
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xmlString));
        Document doc = db.parse(is);

        // optional, but recommended
        // http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        doc.getDocumentElement().normalize();

        return doc;
    }

    protected void createBaseDocument(String URI, String qualifiedName) throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        document = dBuilder.newDocument();
        //add elements to Document
        documentRoot =
                document.createElementNS(URI, qualifiedName);
        //append root element to document
    }

    protected String encodeBase64(String value) {
        return Base64.getEncoder().encodeToString(value.getBytes());
    }

    protected String encodeBase64(byte[] value) {
        return Base64.getEncoder().encodeToString(value);
    }

    protected byte[] decodeBase64Value(String base64Value) {
        return Base64.getDecoder().decode(base64Value);
    }

    public String encryptAndSign()
            throws TransformerException, ParserConfigurationException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            KeyStoreException, UnrecoverableEntryException,
            IOException, CertificateException,
            TransformerException,
            ParserConfigurationException,
            SAXException, MarshalException,
            XMLSignatureException {
        Element xmlEncryptionParameters = encryptionParameter.writeEncryptionParameters();
        documentRoot.appendChild(xmlEncryptionParameters);
        document.appendChild(documentRoot);

        upc.tfm.mpegg.files.utils.Signature sig = new Signature();
        sig.signFile(document, "");

        //for output to file, console
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        DOMSource source = new DOMSource(documentRoot);

        //write data
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);

        transformer.transform(source,result);
        return writer.toString();
    }


    protected abstract void parseXmlData(Document doc);
    public abstract String encrypt(String data, byte[] key) throws InvalidAlgorithmParameterException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
    public abstract String decrypt(String data, byte[] key) throws InvalidAlgorithmParameterException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
    protected String extractXmlTagValue(Document doc, String xmlTag) {
        return doc.getElementsByTagName(xmlTag).item(0).getTextContent();
    }

    protected Cipher generateCipher() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
        String cipherInstance = encryptionParameter.getCipher().getCipherInCipherFormat();
        return Cipher.getInstance(cipherInstance);
    }
}
