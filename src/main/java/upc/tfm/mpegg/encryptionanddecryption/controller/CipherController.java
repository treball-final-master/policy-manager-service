package upc.tfm.mpegg.encryptionanddecryption.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.GetSuiteResponse;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostDecryptionResponse;

import javax.xml.parsers.ParserConfigurationException;
@RestController
public class CipherController {

    @GetMapping("/suite")
    public ResponseEntity<GetSuiteResponse> getCipherSuite() throws ParserConfigurationException {
        return ResponseEntity.ok(new GetSuiteResponse());
    }


}
