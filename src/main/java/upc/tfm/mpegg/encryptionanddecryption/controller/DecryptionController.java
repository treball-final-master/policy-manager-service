package upc.tfm.mpegg.encryptionanddecryption.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestDecryption;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostDatasetDecryptionResponse;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostDatasetGroupDecryptionResponse;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostDecryptionResponse;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.ParserConfigurationException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/decrypt")
public class DecryptionController {

    @PostMapping("/accessunit")
    public ResponseEntity<PostDecryptionResponse> decryptAccessUnit(@RequestBody RequestDecryption requestDecryption) throws ParserConfigurationException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return ResponseEntity.ok(new PostDecryptionResponse(requestDecryption));
    }

    @PostMapping("/dataset")
    public ResponseEntity<PostDatasetDecryptionResponse> decryptDatasetUnit(@RequestBody RequestDecryption requestDatasetDecryption) throws ParserConfigurationException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return ResponseEntity.ok(new PostDatasetDecryptionResponse(requestDatasetDecryption));
    }

    @PostMapping("/datasetgroup")
    public ResponseEntity<PostDatasetGroupDecryptionResponse> decryptDatasetGroupUnit(@RequestBody RequestDecryption requestDatasetDecryption) throws ParserConfigurationException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return ResponseEntity.ok(new PostDatasetGroupDecryptionResponse(requestDatasetDecryption));
    }



}
