package upc.tfm.mpegg.encryptionanddecryption.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestEncryption;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostEncryptionAccessUnitResponse;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostEncryptionDatasetGroupResponse;
import upc.tfm.mpegg.encryptionanddecryption.controller.Responses.PostEncryptionDatasetResponse;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

@RestController
@RequestMapping("/encrypt")
public class EncryptionController {

        @PostMapping("/accessunit")
        public ResponseEntity<PostEncryptionAccessUnitResponse> decryptAccessUnit(@RequestBody RequestEncryption requestEncryption) throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
            return ResponseEntity.ok(new PostEncryptionAccessUnitResponse(requestEncryption));
        }

        @PostMapping("/dataset")
        public ResponseEntity<PostEncryptionDatasetResponse> decryptDataset(@RequestBody RequestEncryption requestEncryption) throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
            return ResponseEntity.ok(new PostEncryptionDatasetResponse(requestEncryption));
        }

        @PostMapping("/datasetgroup")
        public ResponseEntity<PostEncryptionDatasetGroupResponse> decryptDatasetGroup(@RequestBody RequestEncryption requestEncryption) throws MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, ParserConfigurationException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
            return ResponseEntity.ok(new PostEncryptionDatasetGroupResponse(requestEncryption));
        }


}
