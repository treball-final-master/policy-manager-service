package upc.tfm.mpegg.encryptionanddecryption.controller.Responses;

import lombok.Getter;
import lombok.Setter;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;

import java.util.Arrays;


@Getter
@Setter
public class GetSuiteResponse {
    private String[] cipherList;

    public GetSuiteResponse() {
        cipherList = getNames(MPEGG_CIPHERS.class);
    }

    private static String[] getNames(Class<? extends Enum<?>> e) {
        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }
}
