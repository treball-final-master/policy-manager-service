package upc.tfm.mpegg.encryptionanddecryption.controller.Responses;

import lombok.Getter;
import lombok.Setter;
import org.xml.sax.SAXException;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestEncryption;
import upc.tfm.mpegg.files.accessunit.AccessUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;

@Getter
@Setter
public class PostEncryptionAccessUnitResponse {

        private String encryptedData;
        private String protectionBox;
        private String key;

        public PostEncryptionAccessUnitResponse(RequestEncryption requestEncryption) throws ParserConfigurationException, MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
            MPEGG mpegg = new MPEGG();
            AccessUnit au = new AccessUnit(requestEncryption.getCipher());

            mpegg.setAccessUnit(au);
            mpegg.generateKey();

            key = Base64.getMimeEncoder().encodeToString(mpegg.getKey());
            encryptedData = mpegg.encryptAccessUnit(requestEncryption.getData());
            protectionBox = mpegg.signAccessUnit();
        }
}
