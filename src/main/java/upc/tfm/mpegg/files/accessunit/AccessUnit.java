package upc.tfm.mpegg.files.accessunit;

import org.w3c.dom.Document;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

import org.xml.sax.SAXException;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGGProtectionBox;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;


public class AccessUnit extends MPEGGProtectionBox {

    private final String URI = "mpg-access-prot:AccessUnitProtectionType";
    private final String qualifiedName = "AccessUnitProtection";


    public AccessUnit(MPEGG_CIPHERS cipher) throws ParserConfigurationException {
        createBaseDocument(URI, qualifiedName);
        encryptionParameter = new AccessUnitEncryptionParameters(document,cipher);
    }


    public AccessUnit(String XmlFile) throws ParserConfigurationException {
        createBaseDocument(URI, qualifiedName);
        encryptionParameter = new AccessUnitEncryptionParameters(document);
        try {
            Document doc = getXmlFromString(XmlFile);
            parseXmlData(doc);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void parseXmlData(Document doc) {
        byte[] aublockIV = decodeBase64Value(extractXmlTagValue(doc, "aublockIV"));
        encryptionParameter.setIV(aublockIV);
        byte[] auinTAG = decodeBase64Value(extractXmlTagValue(doc, "auinTAG"));
        ((AccessUnitEncryptionParameters) encryptionParameter).setInIV(auinTAG);
        byte[] auinIV = decodeBase64Value(extractXmlTagValue(doc, "auinIV"));
        ((AccessUnitEncryptionParameters) encryptionParameter).setInIV(auinIV);
        byte[] auBlockTAG = decodeBase64Value(extractXmlTagValue(doc, "aublockTAG"));
        encryptionParameter.setTAG(auBlockTAG);
        String cipher = extractXmlTagValue(doc, "cipher");
        encryptionParameter.setCipher(MPEGG_CIPHERS.getFromURI(cipher));
    }








    public void generateValues() {
        encryptionParameter.generateValues();
    }



    @Override
    public String encrypt(String data, byte[] byteKey) throws
            InvalidAlgorithmParameterException, NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {

        Cipher cipher = generateCipher();
        Key key = new SecretKeySpec(byteKey, "AES");
        generateValues();
        AlgorithmParameterSpec algorithmParameterSpec = getParametersSpec();

        cipher.init(Cipher.ENCRYPT_MODE, key, algorithmParameterSpec );
        return encodeBase64(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8)));
    }


    public String decrypt(String data, byte[] byteKey) throws
            InvalidAlgorithmParameterException, NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = generateCipher();

        Key key = new SecretKeySpec(byteKey, "AES");;
        AlgorithmParameterSpec algorithmParameterSpec = getParametersSpec();

        byte [] byteData = decodeBase64Value(data);
        cipher.init(Cipher.DECRYPT_MODE, key, algorithmParameterSpec);

        return new String(cipher.doFinal(byteData));
    }







}
