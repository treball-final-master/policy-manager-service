package upc.tfm.mpegg.files.utils;

import javax.xml.bind.annotation.XmlEnumValue;

public enum MPEGG_CIPHERS {

    @XmlEnumValue("urn:mpeg:mpeg-g:protection:aes128-ctr")
    AES_128_CTR("urn:mpeg:mpeg-g:protection:aes128-ctr"),
    @XmlEnumValue("urn:mpeg:mpeg-g:protection:aes192-ctr")
    AES_192_CTR("urn:mpeg:mpeg-g:protection:aes192-ctr"),
    @XmlEnumValue("urn:mpeg:mpeg-g:protection:aes256-ctr")
    AES_256_CTR("urn:mpeg:mpeg-g:protection:aes256-ctr"),
    @XmlEnumValue("urn:mpeg:mpeg-g:protection:aes128-gcm")
    AES_128_GCM("urn:mpeg:mpeg-g:protection:aes128-gcm"),
    @XmlEnumValue("urn:mpeg:mpeg-g:protection:aes192-gcm")
    AES_192_GCM("urn:mpeg:mpeg-g:protection:aes192-gcm"),
    @XmlEnumValue("urn:mpeg:mpeg-g:protection:aes256-gcm")
    AES_256_GCM("urn:mpeg:mpeg-g:protection:aes256-gcm"),
    @XmlEnumValue("chacha20-poly1305")
    CHACHA_20_POLY_1305("ChaCha20-Poly1305");
    private final String value;

    /**
     * Nova instancia de l'enumeració amb el valor que es passa.
     *
     * @param v Valor de l'enumeració.
     */
    MPEGG_CIPHERS(String v) {
        value = v;
    }

    /**
     * Retorna un CipherURIType a partir d'un valor.
     *
     * @param v Valor a convertir.
     * @return Cipher URI del valor que s'ha passat.
     */
    protected static MPEGG_CIPHERS fromValue(String v) {
        for (MPEGG_CIPHERS c : MPEGG_CIPHERS.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public static MPEGG_CIPHERS getFromURI(String cipher) {
        return fromValue(cipher);
    }

    /**
     * Retorna el valor en format String.
     *
     * @return String del valor del CipherURIType.
     */
    public String value() {
        return value;
    }

    public String getCipherInCipherFormat() {
        switch (this){
            case AES_128_CTR:
            case AES_192_CTR:
            case AES_256_CTR:
                return "AES/CTR/NoPadding";
            case AES_128_GCM:
            case AES_192_GCM:
            case AES_256_GCM:
                return "AES/GCM/NoPadding";
            default:
                return "";
        }
    }

    public int getKeySize() {
        switch (this){
            case AES_128_CTR:
            case AES_128_GCM:
                return 128;
            case AES_192_CTR:
            case AES_192_GCM:
                return 192;
            case AES_256_CTR:
            case AES_256_GCM:
                return 256;
            default: return 0;
        }
    }
}



